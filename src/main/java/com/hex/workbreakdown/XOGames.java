/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.workbreakdown;

import java.util.Scanner;;
/**
 *
 * @author Code.Addict
 */
public class XOGames {
    // + My Scanner ]
    static Scanner requestInput = new Scanner(System.in); 

    // + My.Variables
    static int[] board = new int[] {
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
    };

    static int  round = 1, 
                winner = -1,
                playerTurn = 1;

    // + My short print ]
    static void writeLine(String value) {
        System.out.println(value);
    }

    // + Show Welcome ]
    static void showWelcome() {
        writeLine("Welcome to OX Game");
    }
    
    // + Show Table ]
    static void showTable() {
        String strResult = "\n  1 2 3";
        boolean isNewLine = false;
        int nowRow = 1;

        for (int i = 0; i < board.length; i++) {

            isNewLine = (i % 3 == 0) ? true : false;

            if (isNewLine) {
                strResult += "\n" + nowRow + " ";
                nowRow++;
            }

            strResult += (board[i] == 1 ? 'X' : board[i] == 2 ? 'Y' : '-') + " ";
        }

        writeLine(strResult);
    }
    
    // + Show Turn ]
    static void showTurn() {
        writeLine( (playerTurn == 1 ? 'X' : 'Y') + " turn");
    }

    // + input ]
    static void input() {
        writeLine("Please input Row Col: ");
        int row = requestInput.nextInt(), column = requestInput.nextInt();

        int RowSelected = row * 3 - 1;
        if (RowSelected > 8 || RowSelected < 2 || column < 1 || column > 3) {
            writeLine("\n\n[ !Error ] OX Position input wrong...\n");
            return;
        }

        int PosSecelect = (RowSelected - 2) + (column - 1);
        if (board[PosSecelect] != 0) {
            writeLine("\n\n[ !Error ] Position already assiged before...\n");
            return;
        }

        board[PosSecelect] = round % 2 == 1 ? 1 : 2;

        round += 1;
    }
    
    // + checkWin ]
    static void checkWin() {
        if (round < 3) {
            return;
        }

        winner = (board[0] == playerTurn && board[1] == playerTurn && board[2] == playerTurn) ? playerTurn :
            (board[3] == playerTurn && board[4] == playerTurn && board[5] == playerTurn) ? playerTurn :
            (board[6] == playerTurn && board[7] == playerTurn && board[8] == playerTurn) ? playerTurn :
            (board[0] == playerTurn && board[3] == playerTurn && board[6] == playerTurn) ? playerTurn :
            (board[1] == playerTurn && board[4] == playerTurn && board[7] == playerTurn) ? playerTurn :
            (board[2] == playerTurn && board[5] == playerTurn && board[8] == playerTurn) ? playerTurn :
            (board[0] == playerTurn && board[4] == playerTurn && board[8] == playerTurn) ? playerTurn :
            (board[2] == playerTurn && board[4] == playerTurn && board[6] == playerTurn) ? playerTurn : round > 9 ? 3 : 0;
    }

    // + switchPlayer ]
    static void switchPlayer() {
        playerTurn = round % 2 == 1 ? 1 : 2;
    }
    
    // + showResult ]
    static void showResult() {
        showTable();
        writeLine(winner == 3 ? "Draw Game :D" : "Player " + (winner % 2 == 1 ? 'X' : 'Y') + " Win....");
    }
    
    // + showBye ]
    static void showBye() {
        writeLine("Bye bye ....");
    }

    public static void main(String[] args) {

        showWelcome();

        do {

            showTable();
            
            showTurn();
            
            input();

            checkWin();
            
            switchPlayer();

        } while (winner < 1);
        
        showResult();

        showBye();

    }
    
}
